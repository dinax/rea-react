
window.addEventListener('load', init);

var inputAltura = document.querySelector('#altura');
var inputPeso = document.querySelector('#peso');
var msg = document.querySelector('#msg');
var imc = 0;

function init() {
  altura.addEventListener('input', react);
  peso.addEventListener('input', react);
  react();
}

function react() {    
    var altura = inputAltura.value
    var peso = inputPeso.value

    if (altura  < 1 || altura  > 3) { 
        msg.innerHTML = "Informe uma altura válida!"
    }else if(peso < 1 || peso > 150) {                       
        msg.innerHTML = "Informe uma peso válido!"
    }else{
    if (altura > 0 && peso > 0){  
        imc = (peso / (altura * altura));
        if (imc < 19) {
        status = "MAGREZA"
        }else  if (imc > 18 && imc < 25) {
        status = "NORMAL"
        }else  if (imc > 24 && imc < 30) {
        status = "SOBREPESO"
        }else  if (imc > 29 && imc < 40) {
        status = "OBESIDADE"
        }else {
        status = "OBESIDADE GRAVE"
        }
        msg.innerHTML = "Seu imc é: " + imc.toFixed(2) + " Seu estado é: " + status
    }
}

}
